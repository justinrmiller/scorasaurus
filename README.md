scorasaurus
===========

Check out the scripts directory for usage info.

To build: mvn clean package

To run:

+   cd target
+   java -jar <scorasaurus jar> server ../service.yml

