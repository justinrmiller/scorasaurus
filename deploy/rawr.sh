export PATH_TO_SCORASAURUS_JAR=/Users/justin/Development/Projects/scorasaurus/target
export PATH_TO_CONFIG_FILE=/Users/justin/Development/Projects/scorasaurus
export JAR_NAME=scorasaurus-0.1-SNAPSHOT.jar
export CONFIG_FILE=service.yml
export SERVER_IP=$1

if [[ $# -ne 1 ]]
then
  echo "error: must pass server IP."
  exit -1
fi

if [ ! -f $PATH_TO_SCORASAURUS_JAR/$JAR_NAME ]; then
    echo "Can't find Scorasaurus jar..."
    exit -1
fi

if [ ! -f $PATH_TO_CONFIG_FILE/$CONFIG_FILE ]; then
    echo "Can't find config file..."
    exit -1
fi

echo "
               __   - Let's ship this to $SERVER_IP
              / _)
     _/\/\/\_/ /
   _|         /
 _|  (  | (  |
/__.-'|_|--|_|
"

echo "Deploying $JAR_NAME..."
echo ""

echo "Halting Scorasaurus..."
ssh root@$SERVER_IP "pkill -9 -f java"

echo "Uploading Scorasaurus jar..."
scp $PATH_TO_SCORASAURUS_JAR/$JAR_NAME root@$SERVER_IP:/root

echo "Uploading config file..."
scp $PATH_TO_CONFIG_FILE/$CONFIG_FILE root@$SERVER_IP:/root

echo "Starting Scorasaurus..."
ssh -f root@$SERVER_IP "nohup java -jar /root/$JAR_NAME server /root/$CONFIG_FILE < /dev/null > std.out 2> std.err &"