package com.justinrmiller.scorasaurus.model;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.justinrmiller.scorasaurus.ScorasaurusConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.reflections.Reflections;

import static org.junit.Assert.assertEquals;

/**
 * @author jmiller
 */
@RunWith(MockitoJUnitRunner.class)
public class ModelContainerTest {
    private final static String MODEL_PACKAGE = "com.justinrmiller.scorasaurus.model.testmodels";

    @Test
    public void expectEmptyModelContainerWhenNoModelsProvider() throws Exception {
        final ModelContainer emptyModelContainer = new ModelContainer(ImmutableSet.<Class<? extends Model>>of());
        assertEquals(0, emptyModelContainer.getModels().size());
    }

    @Test
    public void expectOneModelInModelContainerWhenAModelIsProvided() throws Exception {
        ImmutableSet<Class<? extends Model>> classes
            = ImmutableSet.copyOf(new Reflections(MODEL_PACKAGE).getSubTypesOf(Model.class));

        final ModelContainer modelContainer = new ModelContainer(classes);
        assertEquals(1, modelContainer.getModels().size());
        assertEquals("constant", modelContainer.getModels().get("constant").getName());
    }
}
