package com.justinrmiller.scorasaurus.model.testmodels;

import com.fasterxml.jackson.databind.JsonNode;
import com.justinrmiller.scorasaurus.core.ScoreResult;
import com.justinrmiller.scorasaurus.core.ValidationResult;
import com.justinrmiller.scorasaurus.model.Model;

import java.util.Map;

/**
 * @author jmiller
 */
public class ConstantModel implements Model {
    final static String MODEL_NAME = "constant";

    public Long constant = 1L;

    @Override
    public String getName() {
        return MODEL_NAME;
    }

    @Override
    public ValidationResult validate(final Map<String, JsonNode> input) {
        return new ValidationResult(new ValidationResult.Success());
    }

    @Override
    public ScoreResult score(final Map<String, JsonNode> input) {
        return new ScoreResult(new ScoreResult.LongScore(constant));
    }
}