package com.justinrmiller.scorasaurus;

import io.dropwizard.Configuration;

/**
 * @author Justin Miller (Copyright 2014)
 */
public class ScorasaurusConfiguration extends Configuration {
    private final static String MODEL_IMPL_PACKAGE_NAME = "com.justinrmiller.scorasaurus.model.models";

    public static String getModelImplPackageName() { return MODEL_IMPL_PACKAGE_NAME; }
}