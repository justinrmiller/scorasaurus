package com.justinrmiller.scorasaurus.resource;

import com.fasterxml.jackson.databind.JsonNode;

import com.google.inject.Inject;
import com.google.inject.Provider;

import com.justinrmiller.scorasaurus.core.Input;
import com.justinrmiller.scorasaurus.core.ScoreResult;
import com.justinrmiller.scorasaurus.core.ValidationResponse;
import com.justinrmiller.scorasaurus.core.ValidationResult;

import com.justinrmiller.scorasaurus.scorer.Scorer;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Map;

/**
 * @author Justin Miller (Copyright 2014)
 */
@Path("/v1.0")
@Produces(MediaType.APPLICATION_JSON)
public class ScorasaurusResource {
    private final Provider<Scorer> scorerProvider;

    @Inject
    public ScorasaurusResource(final Provider<Scorer> scorerProvider) {
        this.scorerProvider = scorerProvider;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/score")
    public Response score(final Input input) {
        final Scorer scorer = scorerProvider.get();
        final String modelName = input.getModelInfo().getName();
        final Map<String, JsonNode> data = input.getData();

        final ValidationResult validated = scorer.validate(modelName, data);

        if (validated.getResult() instanceof ValidationResult.Success) {
            ScoreResult computedScore = scorer.score(modelName, data);

            return Response.ok(computedScore).build();
        } else {
            return Response.serverError()
                .status(Response.Status.BAD_REQUEST)
                .entity(new ValidationResponse(validated))
                .build();
        }
    }
}
