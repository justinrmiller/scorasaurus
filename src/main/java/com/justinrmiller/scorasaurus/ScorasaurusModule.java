package com.justinrmiller.scorasaurus;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.justinrmiller.scorasaurus.resource.ScorasaurusResource;
import com.justinrmiller.scorasaurus.scorer.Scorer;
import com.justinrmiller.scorasaurus.scorer.ScorerImpl;

/**
 * @author Justin Miller (Copyright 2014)
 */
public class ScorasaurusModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(ScorasaurusResource.class);
        bind(Scorer.class).to(ScorerImpl.class);
    }

    @Provides
    @Named("configuration")
    public ScorasaurusConfiguration provideConfiguration(ScorasaurusConfiguration configuration) {
        return configuration;
    }
}