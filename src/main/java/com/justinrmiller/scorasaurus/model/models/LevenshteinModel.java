package com.justinrmiller.scorasaurus.model.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.justinrmiller.scorasaurus.core.ScoreResult;
import com.justinrmiller.scorasaurus.core.ValidationResult;
import com.justinrmiller.scorasaurus.model.Model;

import java.util.Map;

/**
 * @author Justin Miller (Copyright 2014)
 */
public class LevenshteinModel implements Model {
    private final static String MODEL_NAME = "levenshtein";

    @Override
    public String getName() {
        return MODEL_NAME;
    }

    @Override
    public ValidationResult validate(final Map<String, JsonNode> input) {
        ImmutableList.Builder<ValidationResult.Error.Cause> causeList = new ImmutableList.Builder<>();

        if (!input.containsKey("a")) {
            causeList.add(new ValidationResult.Error.MissingInformation(ImmutableList.of("a")));
        }
        if (!input.containsKey("b")) {
            causeList.add(new ValidationResult.Error.MissingInformation(ImmutableList.of("b")));
        }

        ImmutableList<ValidationResult.Error.Cause> causes = causeList.build();

        if (!causes.isEmpty()) {
            return new ValidationResult(new ValidationResult.Error(causes));
        } else {
            return new ValidationResult(new ValidationResult.Success());
        }
    }

    @Override
    public ScoreResult score(final Map<String, JsonNode> input) {
        Preconditions.checkNotNull(input);

        return new ScoreResult(
            new ScoreResult.IntegerScore(
                levenshteinDistance(
                        input.get("a").asText(),
                        input.get("b").asText()
                )
            )
        );
    }

    // source: http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Java
    static int levenshteinDistance(String s0, String s1) {
        int len0 = s0.length() + 1;
        int len1 = s1.length() + 1;

        // the array of distances
        int[] cost = new int[len0];
        int[] newcost = new int[len0];

        // initial cost of skipping prefix in String s0
        for (int i = 0; i < len0; i++) cost[i] = i;

        // dynamically computing the array of distances

        // transformation cost for each letter in s1
        for (int j = 1; j < len1; j++) {
            // initial cost of skipping prefix in String s1
            newcost[0] = j;

            // transformation cost for each letter in s0
            for(int i = 1; i < len0; i++) {
                // matching current letters in both strings
                int match = (s0.charAt(i - 1) == s1.charAt(j - 1)) ? 0 : 1;

                // computing cost for each transformation
                int cost_replace = cost[i - 1] + match;
                int cost_insert  = cost[i] + 1;
                int cost_delete  = newcost[i - 1] + 1;

                // keep minimum cost
                newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
            }

            // swap cost/newcost arrays
            int[] swap = cost; cost = newcost; newcost = swap;
        }

        // the distance is the cost for transforming all letters in both strings
        return cost[len0 - 1];
    }
}