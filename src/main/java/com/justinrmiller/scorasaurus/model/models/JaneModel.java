package com.justinrmiller.scorasaurus.model.models;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Map;
import java.util.Random;

import com.justinrmiller.scorasaurus.core.ScoreResult;
import com.justinrmiller.scorasaurus.core.ValidationResult;
import com.justinrmiller.scorasaurus.model.Model;

/**
 * @author Justin Miller (Copyright 2014)
 *
 * This model ensures that every time a request is made, a different value from a model can be returned. If this were
 * the same every time, it would mean a brand new ModelContainer for each request wasn't being provided,
 * which could result in thread safety issues as there's no synchronization/etc.
 *
 * The value returned should be different each time, regardless of input.
 */
public class JaneModel implements Model {
    private final static String MODEL_NAME = "jane";

    private final double rand = new Random(System.nanoTime()).nextDouble();

    @Override
    public String getName() {
        return MODEL_NAME;
    }

    @Override
    public ValidationResult validate(final Map<String, JsonNode> input) {
        return new ValidationResult(new ValidationResult.Success());
    }

    @Override
    public ScoreResult score(final Map<String, JsonNode> input) {
        return new ScoreResult(new ScoreResult.DoubleScore(rand));
    }
}
