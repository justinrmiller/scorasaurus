package com.justinrmiller.scorasaurus.model.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.justinrmiller.scorasaurus.core.ScoreResult;
import com.justinrmiller.scorasaurus.core.ValidationResult;
import com.justinrmiller.scorasaurus.model.Model;

import java.util.Map;

/**
 * @author Justin Miller (Copyright 2014)
 */
public class TarzanModel implements Model {
    private final static String MODEL_NAME = "tarzan";

    @Override
    public String getName() {
        return MODEL_NAME;
    }

    @Override
    public ValidationResult validate(final Map<String, JsonNode> input) {
        ImmutableList.Builder<ValidationResult.Error.Cause> causeList = new ImmutableList.Builder<>();

        if (input.containsKey("user1") && input.containsKey("user2")) {
            JsonNode user1 = input.get("user1"), user2 = input.get("user2");

            if (user1 != null && user2 != null) {
                String [] user1Questions = {"question_1", "question_2", "question_3"};
                String [] user2Questions = {"question_1", "question_2", "question_3"};

                for (String question: user1Questions) {
                    if (!user1.has(question)) {
                        causeList.add(new ValidationResult.Error.MissingInformation(ImmutableList.of("user1." + question)));
                    }
                }

                for (String question: user2Questions) {
                    if (!user2.has(question)) {
                        causeList.add(new ValidationResult.Error.MissingInformation(ImmutableList.of("user2." + question)));
                    }
                }
            }
        } else {
            if (!input.containsKey("user1")) { causeList.add(new ValidationResult.Error.MissingInformation(ImmutableList.of("user1"))); }
            if (!input.containsKey("user2")) { causeList.add(new ValidationResult.Error.MissingInformation(ImmutableList.of("user2"))); }
        }

        ImmutableList<ValidationResult.Error.Cause> causes = causeList.build();

        if (!causes.isEmpty()) {
            return new ValidationResult(new ValidationResult.Error(causes));
        } else {
            return new ValidationResult(new ValidationResult.Success());
        }
    }

    @Override
    public ScoreResult score(final Map<String, JsonNode> input) {
        Preconditions.checkNotNull(input);

        final int diffq1 = (input.get("user1").get("question_1").asInt() - input.get("user2").get("question_1").asInt());
        final int diffq2 = (input.get("user1").get("question_2").asInt() - input.get("user2").get("question_2").asInt());
        final int diffq3 = (input.get("user1").get("question_3").asInt() - input.get("user2").get("question_3").asInt());

        final double diffsquaredq1 = diffq1 * diffq1;
        final double diffsquaredq2 = diffq2 * diffq2;
        final double diffsquaredq3 = diffq3 * diffq3;

        double score = Math.sqrt(diffsquaredq1 + diffsquaredq2 + diffsquaredq3);

        return new ScoreResult(
            new ScoreResult.DoubleScore(score),
            ImmutableMap.of(
                "question_1", new ScoreResult.IntegerScore(diffq1),
                "question_2", new ScoreResult.IntegerScore(diffq2),
                "question_3", new ScoreResult.IntegerScore(diffq3)
            ));
    }
}
