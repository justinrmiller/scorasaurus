package com.justinrmiller.scorasaurus.model.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.justinrmiller.scorasaurus.core.ScoreResult;
import com.justinrmiller.scorasaurus.core.ValidationResult;
import com.justinrmiller.scorasaurus.model.Model;

import java.util.Map;

/**
 * @author Justin Miller (Copyright 2014)
 */
public class ConstantBooleanModel implements Model {
    private final static String MODEL_NAME = "constant_boolean";

    private final Boolean constant = Boolean.TRUE;

    @Override
    public String getName() {
        return MODEL_NAME;
    }

    @Override
    public ValidationResult validate(final Map<String, JsonNode> input) {
        return new ValidationResult(new ValidationResult.Success());
    }

    @Override
    public ScoreResult score(final Map<String, JsonNode> input) {
        return new ScoreResult(new ScoreResult.BooleanScore(constant));
    }
}
