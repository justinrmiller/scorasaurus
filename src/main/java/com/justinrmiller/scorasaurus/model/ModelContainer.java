package com.justinrmiller.scorasaurus.model;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.justinrmiller.scorasaurus.ScorasaurusConfiguration;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

/**
 * @author Justin Miller (Copyright 2014)
 */
public class ModelContainer {
    private final static Logger logger = LoggerFactory.getLogger(ModelContainer.class);

    private ImmutableMap<String, Model> models;

    // such optimization, many performance gains
    private final static ImmutableSet<Class<? extends Model>> classes =
            ImmutableSet
                .copyOf(new Reflections(ScorasaurusConfiguration.getModelImplPackageName())
                .getSubTypesOf(Model.class));

    public ModelContainer() {
        this(classes);
    }

    /* package */ ModelContainer(final ImmutableSet<Class<? extends Model>> classes) {
        ImmutableMap.Builder<String, Model> modelsBuilder = new ImmutableMap.Builder<>();

        if (classes != null) {
            for (Class<? extends Model> foundClass : classes) {
                try {
                    Model model = foundClass.newInstance();
                    modelsBuilder.put(model.getName(), model);
                } catch (Exception ex) {
                    logger.error("Error loading model: ", foundClass.getCanonicalName(), ex);
                }
            }
        }

        models = modelsBuilder.build();
    }

    public ImmutableMap<String, Model> getModels() {
        return models;
    }
}
