package com.justinrmiller.scorasaurus.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.justinrmiller.scorasaurus.core.ScoreResult;
import com.justinrmiller.scorasaurus.core.ValidationResult;

import java.util.Map;

/**
 * @author Justin Miller (Copyright 2014)
 */
public interface Model {
    String getName();
    ValidationResult validate(Map<String, JsonNode> objectNode);
    ScoreResult score(Map<String, JsonNode>  objectNode);
}
