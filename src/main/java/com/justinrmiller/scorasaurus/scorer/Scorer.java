package com.justinrmiller.scorasaurus.scorer;

import com.fasterxml.jackson.databind.JsonNode;
import com.justinrmiller.scorasaurus.core.ScoreResult;
import com.justinrmiller.scorasaurus.core.ValidationResult;

import java.util.Map;

/**
 * @author Justin Miller (Copyright 2014)
 */
public interface Scorer {
    ScoreResult score(String modelName, Map<String, JsonNode> data);
    ValidationResult validate(String modelName, Map<String, JsonNode>  data);
}