package com.justinrmiller.scorasaurus.scorer;

import com.fasterxml.jackson.databind.JsonNode;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.justinrmiller.scorasaurus.core.ScoreResult;
import com.justinrmiller.scorasaurus.core.ValidationResult;
import com.justinrmiller.scorasaurus.model.Model;
import com.justinrmiller.scorasaurus.model.ModelContainer;

import java.util.Map;

/**
 * @author Justin Miller (Copyright 2014)
 */
public class ScorerImpl implements Scorer {
    private final ModelContainer modelContainer;

    private final static String DATA_MISSING = "data_missing";

    @Inject
    public ScorerImpl(final ModelContainer modelContainer) {
        this.modelContainer = modelContainer;
    }

    @Override
    public ScoreResult score(final String modelName, final Map<String, JsonNode> data) {
        final Model model = modelContainer.getModels().get(modelName);

        return model.score(data);
    }

    @Override
    public ValidationResult validate(final String modelName, final Map<String, JsonNode> data) {
        ImmutableList.Builder<ValidationResult.Error.Cause> causeBuilder = ImmutableList.builder();

        if (data == null) {
            causeBuilder.add(new ValidationResult.Error.MissingInformation(ImmutableList.of(DATA_MISSING)));
        }

        final Model model = modelContainer.getModels().get(modelName);

        if (model == null) {
            causeBuilder.add(new ValidationResult.Error.MissingModel(modelName));
        }

        ImmutableList<ValidationResult.Error.Cause> causes = causeBuilder.build();

        if (!causes.isEmpty()) {
            final ValidationResult.Error error = new ValidationResult.Error(causes);

            return new ValidationResult(error);
        } else {
            return model.validate(data);
        }
    }
}
