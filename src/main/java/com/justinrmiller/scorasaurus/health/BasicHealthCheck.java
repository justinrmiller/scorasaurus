package com.justinrmiller.scorasaurus.health;

import com.codahale.metrics.health.HealthCheck;

/**
 * @author Justin Miller (Copyright 2014)
 */
public class BasicHealthCheck extends HealthCheck {
    public BasicHealthCheck() {
    }

    @Override
    protected Result check() {
        return Result.healthy();
    }
}