package com.justinrmiller.scorasaurus;

import com.google.inject.Guice;
import com.google.inject.Injector;

import com.justinrmiller.scorasaurus.health.BasicHealthCheck;
import com.justinrmiller.scorasaurus.resource.ScorasaurusResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * @author Justin Miller (Copyright 2014)
 */
public class ScorasaurusApplication extends Application<ScorasaurusConfiguration> {
    private static final String SERVICE_NAME = "scorasaurus";

    @Override
    public String getName() {
        return SERVICE_NAME;
    }

    @Override
    public void initialize(Bootstrap<ScorasaurusConfiguration> bootstrap) {
    }

    @Override
    public void run(ScorasaurusConfiguration configuration, Environment environment) {
        environment.healthChecks().register("basic", new BasicHealthCheck());

        Injector scorasaurusInjector = Guice.createInjector(new ScorasaurusModule());

        environment.jersey().register(scorasaurusInjector.getInstance(ScorasaurusResource.class));
    }

    public static void main(String[] args) throws Exception {
        new ScorasaurusApplication().run(args);
    }
}