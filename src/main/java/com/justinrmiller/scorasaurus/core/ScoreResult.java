package com.justinrmiller.scorasaurus.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.dropwizard.jackson.JsonSnakeCase;

import java.util.Map;

/**
 * @author jmiller
 */
@JsonSnakeCase
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class ScoreResult {
    private static interface Score {}

    @JsonSnakeCase
    public static class BooleanScore implements Score {
        @JsonProperty private Boolean score;

        public BooleanScore() {
            // needed for deserialization
        }

        public BooleanScore(final Boolean score) {
            this.score = score;
        }

        public Boolean getScore() {
            return score;
        }
    }

    @JsonSnakeCase
    public static class LongScore implements Score {
        @JsonProperty private Long score;

        public LongScore() {
            // needed for deserialization
        }

        public LongScore(final Long score) {
            this.score = score;
        }

        public Long getScore() {
            return score;
        }
    }

    @JsonSnakeCase
    public static class DoubleScore implements Score {
        @JsonProperty private Double score;

        public DoubleScore() {
            // needed for deserialization
        }

        public DoubleScore(final Double score) {
            this.score = score;
        }

        public Double getScore() {
            return score;
        }
    }

    @JsonSnakeCase
    public static class IntegerScore implements Score {
        @JsonProperty private Integer score;

        public IntegerScore() {
            // needed for deserialization
        }

        public IntegerScore(final Integer score) {
            this.score = score;
        }

        public Integer getScore() {
            return score;
        }
    }

    @JsonSnakeCase
    public static class StringScore implements Score {
        @JsonProperty private String score;

        public StringScore() {
            // needed for deserialization
        }

        public StringScore(final String score) {
            this.score = score;
        }

        public String getScore() {
            return score;
        }
    }

    @JsonProperty private Score score;
    @JsonProperty private Map<String, ? extends Score> subScores;

    public ScoreResult() {
        // needed for deserialization
    }

    public ScoreResult(final Score score) {
        this.score = score;
    }

    public ScoreResult(final Score score, final Map<String, ? extends Score> subScores) {
        this.score = score;
        this.subScores = subScores;
    }
}
