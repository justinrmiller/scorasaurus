package com.justinrmiller.scorasaurus.core;

import io.dropwizard.jackson.JsonSnakeCase;

/**
 * @author Justin Miller (Copyright 2014)
 */
@JsonSnakeCase
public class ValidationResponse {
    private ValidationResult validationResult;

    public ValidationResponse() {
        // needed for deserialization
    }

    public ValidationResponse(final ValidationResult validationResult) {
        this.validationResult = validationResult;
    }

    public ValidationResult getValidationResult() {
        return validationResult;
    }
}
