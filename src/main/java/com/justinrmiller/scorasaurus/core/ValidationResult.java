package com.justinrmiller.scorasaurus.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.google.common.collect.ImmutableList;
import io.dropwizard.jackson.JsonSnakeCase;

import java.util.List;

/**
 * @author Justin Miller (Copyright 2014)
 */
@JsonSnakeCase
public class ValidationResult {
    private Result result;

    private interface Result {}

    @JsonSnakeCase
    public static class Success implements Result {
        @JsonProperty
        private String status = "success";
    }

    @JsonSnakeCase
    public static class Error implements Result {
        @JsonProperty
        private String status = "error";

        public static interface Cause {}

        @JsonSnakeCase
        public static class MissingModel implements Cause {
            @JsonProperty
            private String type = "missing_model";

            @JsonProperty
            private String name;

            public MissingModel() {
                // needed for deserialization
            }

            public MissingModel(final String name) {
                this.name = name;
            }

            public String getName() {
                return name;
            }
        }

        @JsonSnakeCase
        public static class MissingInformation implements Cause {
            @JsonProperty
            private String type = "missing_information";

            @JsonProperty
            private List<String> fields;

            public MissingInformation() {
                // necessary for deserialization
            }

            public MissingInformation(final List<String> fields) {
                this.fields = fields;
            }

            public List<String> getFields() {
                return fields;
            }
        }

        @JsonProperty("causes")
        private List<Cause> causes;

        public Error() {
            // necessary for deserialization
        }

        public Error(final List<Cause> causes) {
            this.causes = causes;
        }
    }

    public ValidationResult() {
        // necessary for deserialization
    }

    public ValidationResult(final Result result) {
        this.result = result;
    }

    public Result getResult() { return result; }
}