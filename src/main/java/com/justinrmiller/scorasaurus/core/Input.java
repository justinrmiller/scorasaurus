package com.justinrmiller.scorasaurus.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

import io.dropwizard.jackson.JsonSnakeCase;

import java.util.Map;

/**
 * @author Justin Miller (Copyright 2014)
 */
@JsonSnakeCase
public class Input {
    @JsonSnakeCase
    public static class ModelInfo {
        private String name;

        public ModelInfo() {
            // needed for deserialization
        }

        public ModelInfo(final String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    @JsonProperty("model")
    private ModelInfo modelInfo;

    @JsonProperty("data")
    private Map<String, JsonNode> data;

    public Input() {
        // needed for deserialization
    }

    public Input(final ModelInfo modelInfo, final Map<String, JsonNode> data) {
        this.modelInfo = modelInfo;
        this.data = data;
    }

    public ModelInfo getModelInfo() {
        return modelInfo;
    }

    public Map<String, JsonNode> getData() {
        return data;
    }
}
